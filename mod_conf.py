# Set location of CDR files
# Example: cdr_path = "/opt/ftproot/"
cdr_path = "/mnt/sdb1/ftproot/"

# Set location where CDR files are archived to after processing
# Example: archive_path = "/tmp/cdr/archive/"
archive_path = "/mnt/sdb1/archive/"

# Set ElasticSearch Index
# Example: es_index = "cdr"
# from datetime import datetime
# es_index = "cdr-%s" % datetime.utcnow().strftime("%Y.%m.%d")
es_index = ""

# Set ElasticSearch Type
# Example: es_type = "parsed_cdr"
es_type = ""
